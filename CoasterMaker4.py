# -*- coding: utf-8 -*-
'''
*
*    CoasterMaker4.py
*
*    A FreeCAD Macro that creates a multiple-materials Coaster with circular text.
*    Useful to create extruded and circular text in addition.  Utilizes
*    FCDialog_.py imported to provide a Dialog with State.
*
*    Copyright 2019 Floating Cameras, LLC - Author: Marc Cole
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*
'''
_macroName = "CoasterMaker4"


import Part, Draft, Mesh
from math import cos, sin, degrees, radians

from os.path import expanduser, isfile, isdir


from PySide import QtCore, QtGui
import FreeCAD, FreeCADGui, traceback
import pickle


import sys, os
from os.path import expanduser, isfile, isdir


class DialogItem:
    def __init__(self, po_field, ps_default):
        self.name = po_field.name
        self.label = po_field.label
        self.field_type = po_field.type
        self.default = ps_default
        self.flags = po_field.flags

        self.qlabel = None
        self.qfield = None

class DFieldDef :
    def __init__(self, ps_name, ps_label, ps_type, ps_default, pi_flags = 0):
        self.name = ps_name
        self.label = ps_label
        self.type = ps_type
        self.default = ps_default
        self.flags = pi_flags



class Ui_Dialog(object):
    def __init__(self, ps_name, pl_fields, po_App, po_Gui, pl_winsize):
        self.name = ps_name
        self.fields_ = pl_fields
        self.app = po_App
        self.gui = po_Gui
        self.winsize = pl_winsize  #width = pl_winsize[0]
        # self.height = pl_winsize[1]

        self._structure_group = None
        self._print_group = None
        self._compound_group = None

        self.smidge = 0.10
        self.smidges = self.smidge * 2


        # Responses are held as strings in a list for saving and restoring State
        self.responses = {}
        for field_ in self.fields_ :
            if field_ :
                self.responses[field_.name] = field_.default

        state_file = "{}\\FreeCAD\\Macro\\{}.state".format(os.getenv("APPDATA"), self.name)
        # If a State file exists open and update the default values in responses
        if isfile(state_file):
            with open(state_file, 'rb') as f:
                # The protocol version used is detected automatically, so we do not
                # have to specify it.
                statevals = pickle.load(f)
                for key in iter(statevals.keys()) :
                    if key in self.responses :
                        self.responses[key] = statevals[key]

        # State has been 'restored'.  Set the default values for input
        self.fields = []
        for field in self.fields_ :
            if field :
                self.fields.append(DialogItem(po_field = field, ps_default=self.responses[field.name]))

    def getValue(self, ps_field_name):
        for i in range(0, len(self.fields)) :
            field = self.fields[i]
            if field.name == ps_field_name :
                if field.field_type == "Text" :
                    return field.qfield.toPlainText()
                elif field.field_type == "Line" :
                    return field.qfield.text()
                elif field.field_type == "Input" :
                    return field.qfield.property("quantity")
                elif field.field_type == "CheckBox" :
                    return field.qfield.isChecked()

        return None

    def print_value(self, name):
        print("{}:{}".format(name, self.getValue(name)))
        # </editor-fold makeIt() Process the completed dialog>

    def establishGroup(self, name) :
        group = self.app.ActiveDocument.getObject(name)
        if group is None :
            group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", name)
        else :
            if self.getValue("DeleteAll") :
                group.removeObjectsFromDocument()   # Delete the contents

        return group

    def exportSTLs(self, root_name):
        # ------------------------
        # -- Export STLs
        # ------------------------
        if self.getValue("CreateOutput") :
            outdir = self.responses["OutputDir"]

            cur_output = self.getValue("OutputDir").strip()

            if cur_output:  # Blanking the output directory prevents output generation but doesn't save

                for item in self._print_group.Group:
                    # cur_output may exist but not be a valid directory. Responses validate the OutDir before changing it
                    filename = u"{}\\{}_{}.stl".format(outdir, root_name, item.Label)
                    Mesh.export([item], filename)
                    print("Exported: {}".format(filename))
            else:
                print("No output files generated...")


    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(self.winsize[0]+100, self.winsize[1] + 35)
        Dialog.setWindowTitle(QtGui.QApplication.translate \
                                  ("Dialog", Dialog.ui.name,
                                   None))
        self.dia = Dialog
        self.gridLayoutWidget = QtGui.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(19, 19, self.winsize[0], self.winsize[1]))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")

        for i in range(0,len(self.fields)) :
            field = self.fields[i]

            field.qlabel = QtGui.QLabel(self.gridLayoutWidget)
            field.qlabel.setObjectName("label_".format(i))
            field.qlabel.setText(QtGui.QApplication.translate \
                               ("Dialog", field.label, None))

            self.gridLayout.addWidget(field.qlabel, i, 0, 1,1)
            if field.field_type == "Text" :
                field.qfield = QtGui.QTextEdit(self.gridLayoutWidget)

            elif field.field_type == "Line" :
                field.qfield = QtGui.QLineEdit(self.gridLayoutWidget)

            elif field.field_type == "Input" :
                fui = FreeCADGui.UiLoader()
                field.qfield = fui.createWidget("Gui::InputField")

            elif field.field_type == "CheckBox" :
                field.qfield = QtGui.QCheckBox(self.gridLayoutWidget)
                # field.qfield.setText(field.label)


            if field.qfield :
                field.qfield.setObjectName("field_".format(i))     # Apply friendly name to qfield
                self.gridLayout.addWidget(field.qfield,i,1,1,1)    # Add field to a specific cell
                if field.default :
                    if field.field_type == "CheckBox" :
                        field.qfield.setChecked(True if field.default == "True" else False)
                    else:
                        field.qfield.setText(field.default)

        self.buttonBox = QtGui.QDialogButtonBox(self.gridLayoutWidget)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons \
            (QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, len(self.fields), 1, 1, 1)

        QtCore.QObject.connect(self.buttonBox, \
                               QtCore.SIGNAL("accepted()"), self.makeSomething)
        QtCore.QObject.connect(self.buttonBox, \
                               QtCore.SIGNAL("rejected()"), self.makeNothing)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


    def makeSomething(self):
        if self.validate_responses()  :
            self.dia.close()
            self.makeIt()
            self.app.ActiveDocument.recompute()

            # self.export(docName="override makeSomething()" )

            self.saveState()

            self.gui.SendMsgToActiveView("ViewFit")
            self.app.ActiveDocument.recompute()

        else :
            print ("validation failed")

    def makeNothing(self):
        print ("rejected!!")
        self.dia.close()

    def makeIt(self):
        print ("Override makeIt() to use this Class")

    def validate_responses(self):
        '''
        Moves responses from Gui to responses dictionary. This is where
        response validation can be implemented.

        :return: True when all responses are valid, False if user needs to correct a value
        '''
        is_valid = True

        for i in range(0, len(self.fields)) :
            field = self.fields[i]
            if field.flags == 0 :
                self.responses[field.name] = str(self.getValue(field.name))
            elif field.flags == 1 :
                rcheck = self.getValue(field.name)
                if rcheck != field.default and rcheck.strip() != "" and isfile(rcheck) :
                    self.responses[field.name] = rcheck
            elif field.flags == 2 :
                rcheck = self.getValue(field.name)
                if rcheck != field.default and rcheck.strip() != "" and isdir(rcheck) :
                    self.responses[field.name] = rcheck
            else :
                self.responses[field.name] = str(self.getValue(field.name))


        return is_valid

    # ----------
    @staticmethod
    def say(s):
        FreeCAD.Console.PrintMessage(str(s) + "\n")

    @staticmethod
    def sayexc(mess=''):
        exc_type, exc_value, exc_traceback = sys.exc_info()
        ttt = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
        lls = eval(ttt)
        FreeCAD.Console.PrintError(mess + "\n" + "-->  ".join(lls))

    @staticmethod
    def close_if_open(ps_docName):
        #  Cleanup -- Close project if it already is active to rebuild it
        if ps_docName in FreeCAD.listDocuments():
            FreeCAD.closeDocument(ps_docName)  # Close the application if opened

    def saveState(self):
        state_file = "{}\\FreeCAD\\Macro\\{}.state".format(os.getenv("APPDATA"), self.name)
        print ("Saving state to: {}".format(state_file))
        # state = pickle.dumps(responses)
        with open(state_file, 'wb') as f:
            # Pickle the 'data' dictionary using the highest protocol available.
            pickle.dump(self.responses, f, pickle.HIGHEST_PROTOCOL)

def ShowDialog(po_DialogDef):
    try:
        d = QtGui.QWidget()
        d.ui = po_DialogDef
        d.ui.setupUi(d)
        # d.adjustSize()
        d.show()

    except Exception:
        print (Exception)
        Ui_Dialog.sayexc(str(Exception))




# <editor-fold desc="Globals...">
# ############################################################################################
# ##   GLOBALS
# ############################################################################################

depth = 3.0
backing_depth = 0.60
radius = 40.0
engrave_depth = 0.60
rim_depth = 1.5

top_text = "FLOATING CAM"
bottom_text = "FreeCAD .18"
centered_text = "Coaster\nMaker"
top_font_size = 9.0
top_font_file = "C:/Windows/Fonts/Arial.ttf"
top_binding_angle = 170
top_extrude = 0.6

centered_font_size = 10.0
centered_font_file = top_font_file
centered_extrude = top_extrude

bottom_font_size = 8.0
bottom_font_file = top_font_file
bottom_binding_angle = 130
bottom_extrude = top_extrude

create_coaster = "True"
extruded_text_height = 0.0


workdir = expanduser("~")

fields = [
    # DFieldDef(ps_name="ProjectName", ps_label="Project Name", ps_type="Line", ps_default=_macroName),

    DFieldDef(ps_name="TopText", ps_label="Top text", ps_type="Line", ps_default=top_text),

    DFieldDef(ps_name="CenteredText", ps_label="Centered Text", ps_type="Text", ps_default=centered_text),

    DFieldDef(ps_name="BottomText", ps_label="Bottom Text", ps_type="Line", ps_default=bottom_text),

    DFieldDef(ps_name="TopBindingAngle", ps_label="Top Binding Angle", ps_type="Input",
              ps_default="{:.2f} mm".format(top_binding_angle)),
    DFieldDef(ps_name="BottomBindingAngle", ps_label="Bottom Binding Angle", ps_type="Input",
              ps_default="{:.2f} mm".format(bottom_binding_angle)),

    DFieldDef(ps_name="Radius", ps_label="Base Radius", ps_type="Input", ps_default="{:.2f} mm".format(radius)),
    DFieldDef(ps_name="BaseDepth", ps_label="Base Depth", ps_type="Input", ps_default="{:.2f} mm".format(depth)),
    DFieldDef(ps_name="RimDepth", ps_label="Rim Depth", ps_type="Input", ps_default="{:.2f} mm".format(rim_depth)),
    DFieldDef(ps_name="BackingDepth", ps_label="Backing Depth", ps_type="Input",
              ps_default="{:.2f} mm".format(backing_depth)),

    DFieldDef(ps_name="TopFontSize", ps_label="Top Font Size", ps_type="Input",
              ps_default="{:.2f} mm".format(top_font_size)),
    DFieldDef(ps_name="CenteredFontSize", ps_label="Centered Font Size", ps_type="Input",
              ps_default="{:.2f} mm".format(centered_font_size)),
    DFieldDef(ps_name="BottomFontSize", ps_label="Bottom Font Size", ps_type="Input",
              ps_default="{:.2f} mm".format(bottom_font_size)),

    DFieldDef(ps_name="TopExtrude", ps_label="Top Extrude", ps_type="Input",
              ps_default="{:.2f} mm".format(top_extrude)),
    DFieldDef(ps_name="CenteredExtrude", ps_label="Centered Extrude", ps_type="Input",
              ps_default="{:.2f} mm".format(centered_extrude)),
    DFieldDef(ps_name="BottomExtrude", ps_label="Bottom Extrude", ps_type="Input",
              ps_default="{:.2f} mm".format(bottom_extrude)),
    DFieldDef(ps_name="EngraveDepth", ps_label="Engrave Depth", ps_type="Input",
              ps_default="{:.2f} mm".format(engrave_depth)),

    DFieldDef(ps_name="TopFontFile", ps_label="Top Font File", ps_type="Line", ps_default=top_font_file, pi_flags=1),
    DFieldDef(ps_name="CenteredFontFile", ps_label="Centered Font File", ps_type="Line", ps_default=centered_font_file,
              pi_flags=1),
    DFieldDef(ps_name="BottomFontFile", ps_label="Bottom Font File", ps_type="Line", ps_default=bottom_font_file,
              pi_flags=1),

    DFieldDef(ps_name="ExtrudedTextHeight", ps_label="Extruded Text Height", ps_type="Input",
              ps_default="{:.2f} mm".format(extruded_text_height)),
    DFieldDef(ps_name="OutputDir", ps_label="Output Directory", ps_type="Line", ps_default=workdir, pi_flags=2),
    DFieldDef(ps_name="CreateCoaster", ps_label="Create Coaster", ps_type="CheckBox", ps_default=create_coaster),
]

# </editor-fold desc="Globals...">

# <editor-fold desc="Support Classes..." >
# class ShapeStringE(object):
#     def __init__(self, ps_id, ps_label, po_char_, pi_index):
#         self.shapeStringID = ps_id
#         self.shapeStringLabel = ps_label
#         self.char_ = po_char_
#         self.index = pi_index


# </editor-fold Support Classes>

class MyProject(Ui_Dialog):
    def __init__(self, ps_name, pl_fields, po_App, po_Gui, pl_winsize=[300,540]):
        Ui_Dialog.__init__(self, ps_name=ps_name, pl_fields=pl_fields, po_App=po_App, po_Gui=po_Gui, pl_winsize = pl_winsize)
        self._structure_group = None
        self._print_group = None
        self._compound_group = None

    # <editor-fold desc="Support Methods...">
    def extrudeChar(self, ps_text, pi_index, po_char_, pf_depth, ps_ssid):
        extrudeName = "{}_{}".format(ps_text, "Flat" if pi_index < 0 else ps_text[pi_index])

        extru_ = self.app.ActiveDocument.addObject('Part::Extrusion', extrudeName)  #
        # extru_.Label = extrudeName
        extru_.Base = po_char_

        extru_.DirMode = "Normal"
        extru_.DirLink = None
        extru_.LengthFwd = pf_depth  # 0.000000000000000
        extru_.LengthRev = 0.000000000000000  # layerNames[0][1]
        extru_.Solid = True
        extru_.Reversed = False  # if j == 1 else True
        extru_.Symmetric = False
        extru_.TaperAngle = 0.000000000000000
        extru_.TaperAngleRev = 0.000000000000000

        extru_.Base.ViewObject.hide()

        guie = self.gui.ActiveDocument.getObject(extrudeName)
        # print ("ssid: {}".format(ps_ssid))
        guio = self.gui.ActiveDocument.getObject(ps_ssid)

        if guio and guie:
            guie.ShapeColor = guio.ShapeColor
            guie.LineColor = guio.LineColor
            guie.PointColor = guio.PointColor

        self.app.ActiveDocument.recompute()

        return extru_

    def grouping(self, _group_name):
        _group = self.app.ActiveDocument.getObject(_group_name)
        if _group:
            _group.removeObjectsFromDocument()
            self.app.ActiveDocument.removeObject(_group_name)

        _group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", _group_name)
        self._structure_group.addObject(_group)

        return _group

    def text_group(self, ps_name):
        ptext = self.getValue(ps_name + "Text")
        if ptext == "":
            return None

        extrude_group_name = ptext + "_"
        extrude_group = self.app.ActiveDocument.getObject(extrude_group_name)
        if extrude_group:
            extrude_group.removeObjectsFromDocument()
            self.app.ActiveDocument.removeObject(extrude_group_name)

        extrude_group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", "_" + extrude_group_name)
        self._structure_group.addObject(extrude_group)

        shapeStringName = extrude_group_name + ptext + "_"  # """{}_{}_".format(ptext, ps_suffix)
        str_ = Draft.makeShapeString(String=ptext,
                                     FontFile=self.getValue(ps_name + "FontFile"),
                                     Size=self.getValue(ps_name + "FontSize"),
                                     Tracking=0)
        shapeStringID = str_.Label  # Capture the ID before changing the label
        str_.Label = shapeStringName
        extrude_group.addObject(str_)

        extru_ = self.extrudeChar(ps_text=ptext, pi_index=-1, po_char_=str_,
                                  pf_depth=self.getValue(ps_name + "Extrude"),
                                  ps_ssid=shapeStringID)

        self.app.ActiveDocument.recompute()
        bb = extru_.Shape.BoundBox
        extru_.Placement.Base.x = -bb.XLength / 2
        extru_.Placement.Base.y = -bb.YLength / 2
        self._compound_group.addObject(extru_)

        return extru_  # Completed  - Extrusion is enough grouping

    def flat_text(self):
        text = self.getValue("CenteredText")
        font_size = float(self.getValue("CenteredFontSize"))
        text_spacing = 1.0

        yinc = font_size + text_spacing

        friendly_name = "_" + text.replace("\n","--")

        extrude_group_name = friendly_name
        shape_group_name = friendly_name + "_"

        extrude_group = self.grouping(extrude_group_name)
        shape_group = self.grouping(shape_group_name)
        extruded_text_height = float(self.getValue(("ExtrudedTextHeight")))
        extruded_text_group = None
        if extruded_text_height > 0.0:
            extruded_text_group = self.grouping(friendly_name + "_Extruded")

        #
        #
        #
        # # extrude_group = self.app.ActiveDocument.getObject(extrude_group_name)
        # if extrude_group:
        #     extrude_group.removeObjectsFromDocument()
        #     self.app.ActiveDocument.removeObject(extrude_group_name)
        #
        # extrude_group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", extrude_group_name)
        # self._structure_group.addObject(extrude_group)

        # shape_group = self.app.ActiveDocument.getObject(shape_group_name)
        # if shape_group:
        #     shape_group.removeObjectsFromDocument()
        #     self.app.ActiveDocument.removeObject(shape_group_name)
        #
        # shape_group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", shape_group_name)
        # self._structure_group.addObject(shape_group)

        lines = text.split("\n")
        yos = (((len(lines) - 1) * yinc) / 2.0) #- (font_size / 2)
        # shapes = []
        extrudes = []
        extruded_text = []
        maxX = 0
        for line in lines:
            if line.strip() != "" :
                # Create Shapestring
                shapeStringName = "_" + line + "_"  # """{}_{}_".format(ptext, ps_suffix)
                str_ = Draft.makeShapeString(String=line,
                                             FontFile=self.getValue("CenteredFontFile"),
                                             Size=font_size,
                                             Tracking=0)
                shapeStringID = str_.Label  # Capture the ID before changing the label
                str_.Label = shapeStringName
                shape_group.addObject(str_)
                str_.Placement.Base.x = -str_.Shape.BoundBox.XLength / 2
                str_.Placement.Base.y = -str_.Shape.BoundBox.YLength / 2


                extru_ = self.extrudeChar(ps_text=line, pi_index=-1, po_char_=str_,
                                          pf_depth=self.getValue("CenteredExtrude"),
                                          ps_ssid=shapeStringID)

                extrudes.append(extru_)
                extrude_group.addObject(extru_)
                extru_.Placement.Base.y = yos

                if extruded_text_height > 0.0 :

                    extru_deep = self.extrudeChar(ps_text=line, pi_index=-1, po_char_=str_,
                                              pf_depth=extruded_text_height,
                                              ps_ssid=shapeStringID)

                    extruded_text_group.addObject(extru_deep)
                    extruded_text.append(extru_deep)
                    extru_deep.Placement.Base.y = yos

                # Establish XLength Max
                maxX = max(maxX, extru_.Shape.BoundBox.XLength)

            yos -= str_.Shape.BoundBox.YLength + text_spacing
        self.app.ActiveDocument.recompute()
        # for shape in extrudes:
        #     # xlen = shape.Shape.BoundBox.XLength
        #     # xpos = -(maxX / 2) + ((maxX - xlen) / 2)
        #     shape.Placement.Base.x = -maxX / 2.0

        if extruded_text_height > 0.0 :
            texte_union_name = friendly_name + "_eText"
            texte_union = self.app.ActiveDocument.addObject("Part::MultiFuse", texte_union_name)
            self._compound_group.addObject(texte_union)
            texte_union.Shapes = extruded_text
            texteg = self.gui.ActiveDocument.getObject(texte_union.Label)
            if texteg :
                texteg.Visibility = False

        text_union_name = friendly_name + "_Text"
        text_union = self.app.ActiveDocument.addObject("Part::MultiFuse", text_union_name)
        self._compound_group.addObject(text_union)
        text_union.Shapes = extrudes

        return text_union



    def print_value(self, name):
        print("{}:{}".format(name, self.getValue(name)))

    def ctext_group(self, ps_name, pb_counter_clockwise=False):
        ptext = self.getValue(ps_name + "Text")
        if ptext == "":
            return None

        radius = float(self.getValue("Radius"))
        font_size = float(self.getValue(ps_name + "FontSize"))
        font_file = self.responses[ps_name + "FontFile"]

        extrude_depth = float(self.getValue(ps_name + "Extrude"))

        inset = 2.0
        # shape_strings = []  # Initialize a list for holding the ShapeStringE objects
        pradius = radius - inset if pb_counter_clockwise else radius - (font_size * 1.0)

        ss_group_name = ptext + "_"

        ss_group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", ss_group_name)
        self._structure_group.addObject(ss_group)

        ptext_XLength = 0.0
        binding_angle = float(self.getValue(ps_name + "BindingAngle"))
        d_angle = binding_angle / len(ptext)
        p_angle = 0.0
        group_e_name = ptext
        group_e = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", group_e_name)
        self._structure_group.addObject(group_e)
        te = []

        extruded_text_height = float(self.getValue(("ExtrudedTextHeight")))

        extruded_text_group = None
        if extruded_text_height > 0.0:
            extruded_text_group = self.grouping(group_e_name + "_Extruded")
        tee = []


        for i in range(0, len(ptext)):
            p_angle = p_angle + (d_angle / 2.0)
            shapeStringName = "{}_{}_".format(ptext, ptext[i])

            char_ = Draft.makeShapeString(String=ptext[i], FontFile=font_file, Size=font_size,
                                          Tracking=0)

            if char_.Shape.isNull():  # Test for 'Space' where there is nothing to extrude
                p_angle = p_angle + (d_angle / 2.0)
                continue


            shapeStringID = char_.Label  # The label is the only place this is visible
            char_.Label = shapeStringName  # "{}_{}_".format(ptext,ptext[i])
            # shape_strings.append(ShapeStringE(ps_id=shapeStringID,
            #                                   ps_label=char_.Label,
            #                                   po_char_=char_,
            #                                   pi_index=i)
            #                      )
            ss_group.addObject(char_)

            ptext_XLength += char_.Shape.BoundBox.XLength

            char_.Placement.Base.x = -char_.Shape.BoundBox.XLength / 2
            # char_.Placement.Base.y = -char_.Shape.BoundBox.YLength / 2


            extru_ = self.extrudeChar(ps_text=ptext, pi_index=i, po_char_=char_, pf_depth=extrude_depth,
                                      ps_ssid=shapeStringID)
            group_e.addObject(extru_)
            te.append(extru_)

            pa = 180 + p_angle if pb_counter_clockwise else 180 - p_angle
            x = cos(radians(pa)) * pradius #- (char_.Shape.BoundBox.XLength / 2)
            y = sin(radians(pa)) * pradius

            ca = p_angle - 90 if pb_counter_clockwise else 90 - p_angle
            plm = self.app.Placement(self.app.Vector(x, y, 0), self.app.Rotation(self.app.Vector(0, 0, 1), ca))

            p_angle += d_angle / 2.0  # Move to end of character in prep of next character

            self.app.ActiveDocument.recompute()
            extru_.Placement = plm


            if extruded_text_height > 0.0 :
                extru_deep = self.extrudeChar(ps_text=ptext, pi_index=i, po_char_=char_, pf_depth=extruded_text_height,
                                          ps_ssid=shapeStringID)
                extruded_text_group.addObject(extru_deep)
                tee.append(extru_deep)
                extru_deep.Placement = plm

        self.app.ActiveDocument.recompute()
        union_name = "{}_CircleC".format(self.getValue(ps_name + "Text")) if pb_counter_clockwise else "{}_Circle".format(
            self.getValue(ps_name + "Text"))
        union = self.app.ActiveDocument.addObject("Part::MultiFuse", union_name)
        self._compound_group.addObject(union)

        union.Shapes = te
        ra = 180 - binding_angle if pb_counter_clockwise else binding_angle - 180
        union.Placement.Rotation = self.app.Rotation(self.app.Vector(0, 0, 1), ra / 2)

        if extruded_text_height > 0.0 :
            union2_name = "{}_CircleCE".format(
                self.getValue(ps_name + "Text")) if pb_counter_clockwise else "{}_CircleE".format(
                self.getValue(ps_name + "Text"))
            union2 = self.app.ActiveDocument.addObject("Part::MultiFuse", union2_name)
            self._compound_group.addObject(union2)

            union2.Shapes = tee
            union2g = self.gui.ActiveDocument.getObject(union2.Label)
            if union2g :
                union2g.Visibility = False

            ra = 180 - binding_angle if pb_counter_clockwise else binding_angle - 180
            union2.Placement.Rotation = self.app.Rotation(self.app.Vector(0, 0, 1), ra / 2)

        return union

    def textUnion(self, shapes, counter=1):
        suffix = "" if counter == 1 else "_" + str(counter)
        union_shapes = []
        for s in shapes:
            if s:
                union_shapes.append(s)

        text_union_name = self.responses["TopText"] + "_" + self.responses["CenteredText"] + "_" + self.responses[
            "BottomText"] + suffix
        text_union = self.app.ActiveDocument.addObject("Part::MultiFuse", text_union_name)
        text_union.Shapes = union_shapes
        text_union.Placement.Base.z = -float(self.getValue("EngraveDepth")) if counter == 1 else -0.20

        if counter == 1 :
            self._print_group.addObject(text_union)
        else:
            self._compound_group.addObject(text_union)

        return text_union

    def coaster(self, text_union):

        # Create a solid cylinder for cutting out the radius of the coaster
        radius = float(self.getValue("Radius"))
        base_depth = float(self.getValue("BaseDepth"))
        csolid = self.app.ActiveDocument.addObject("Part::Cylinder", "CoasterSolid")
        self._structure_group.addObject(csolid)
        csolid.Radius = radius
        csolid.Height = base_depth * 3
        self.app.ActiveDocument.recompute()

        # Add a rim with angle for unsupported printing
        rim = self.app.ActiveDocument.addObject("Part::Cone", "CoasterBottomRim")
        self._structure_group.addObject(rim)

        rim_depth = float(self.getValue("RimDepth"))
        rim.Height = base_depth + rim_depth
        rim.Radius1 = radius
        rim.Radius2 = radius + base_depth
        rim.Placement.Base.z = -base_depth #+ inset  # Two 0.20 layers inset

        # Create the blank Coaster base
        coaster_base_name = "CoasterBase"
        coaster_base = self.app.ActiveDocument.addObject("Part::Cut", coaster_base_name)
        self._compound_group.addObject(coaster_base)
        coaster_base.Base = rim
        coaster_base.Tool = csolid

        # Create the engraved coaster bottom
        coaster_bottom_name = "CoasterBottom"
        coaster_bottom = self.app.ActiveDocument.addObject("Part::Cut", coaster_bottom_name)
        self._print_group.addObject(coaster_bottom)
        coaster_bottom.Base = coaster_base
        coaster_bottom.Tool = text_union #_cutout

        gui_te = self.gui.ActiveDocument.getObject(coaster_bottom.Label)
        gui_te.Visibility = True
        self.app.ActiveDocument.recompute()
        gui_te.ShapeColor = (0.3, 1.0, 1.0)

        # Create the backing waffer
        back_waffer = self.app.ActiveDocument.addObject("Part::Cylinder", "BackWaffer")
        self._print_group.addObject(back_waffer)
        back_waffer.Radius = self.getValue("Radius")
        backing_depth = float(self.getValue("BackingDepth"))
        back_waffer.Height = backing_depth
        back_waffer.Placement.Base.z = (-base_depth - backing_depth) #+ inset

        # Make the printable items visible
        gui_bw = self.gui.ActiveDocument.getObject(back_waffer.Label)
        gui_bw.Visibility = True
        self.app.ActiveDocument.recompute()
        gui_bw.ShapeColor = (1.0, 1.0, 1.0)  # White

        gui_tu = self.gui.ActiveDocument.getObject(text_union.Label)
        gui_tu.Visibility = True
        self.app.ActiveDocument.recompute()
        gui_tu.ShapeColor = (0.0, 0.0, 0.0)  # Black

    def exportSTLs(self):
        # ------------------------
        # -- Export STLs
        # ------------------------
        outdir = self.responses["OutputDir"]

        cur_output = self.getValue("OutputDir").strip()

        if cur_output:  # Blanking the output directory prevents output generation but doesn't save

            for item in self._print_group.Group:
                # cur_output may exist but not be a valid directory. Responses validate the OutDir before changing it
                filename = u"{}\\{}_{}.stl".format(outdir, _macroName, item.Label)
                Mesh.export([item], filename)
                print("Exported: {}".format(filename))

        else:
            print("No output files generated...")
        # # =====================================================================================
        # </editor-fold makeIt() Process the completed dialog>


    # </editor-fold Support Methods>

    # ##############################################################
    # Override -> makeIt()
    # ##############################################################
    def makeIt(self):
        # <editor-fold desc="OVERRIDE makeIt() - Process the completed dialog...">
        if self.app.ActiveDocument is None:
            cname = _macroName  # "Unnamed"
            # print ("Creating Doc")
            doc = self.app.newDocument(cname)
            # self.app.setActiveDocument(cname)
            self.app.ActiveDocument = self.app.getDocument(cname)
            self.gui.ActiveDocument = self.gui.getDocument(cname)

        inset = float(self.getValue("EngraveDepth"))

        # # # Display values collected
        # for fld in self.fields :
        #     self.print_value (fld.name)

        self._print_group = self.app.ActiveDocument.getObject("_Print_")
        if self._print_group is None:
            self._print_group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", "_Print_")
        else:
            self._print_group.removeObjectsFromDocument()

        self._compound_group = self.app.ActiveDocument.getObject("_Compounds_")
        if self._compound_group:
            self._compound_group.removeObjectsFromDocument()
        else:
            self._compound_group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", "_Compounds_")

        self._structure_group = self.app.ActiveDocument.getObject("_Structure_")
        if self._structure_group:
            self._structure_group.removeObjectsFromDocument()
        else:
            self._structure_group = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", "_Structure_")

        top__ = self.ctext_group(ps_name="Top")

        # centered__ = self.text_group(ps_name="Centered")
        centered__ = self.flat_text()

        bottom__ = self.ctext_group(ps_name="Bottom", pb_counter_clockwise=True)

        # With all of the Structure created for the text create some compounds
        # Compound a double decker of all the text to assure cuts are above the surface
        text_union = self.textUnion([top__, centered__, bottom__])  # Create _Print_ of all Text
        # text_union2 = self.textUnion([top__, centered__, bottom__], counter=0)  # Create _Print_ of all Text
        # text_union_cutout_name = self.responses["TopText"] + "_" + self.responses["CenteredText"] + "_" + self.responses[
        #     "BottomText"] + "_Cut"
        # text_union_cutout = self.app.ActiveDocument.addObject("Part::MultiFuse", text_union_cutout_name)
        # self._compound_group.addObject(text_union_cutout)
        # text_union_cutout.Shapes = [text_union, text_union2]

        if self.getValue("CreateCoaster")  :
            self.coaster(text_union)
            self.exportSTLs()


# --------------
ShowDialog(po_DialogDef=MyProject(ps_name=_macroName, pl_fields=fields, po_App=App, po_Gui=Gui, pl_winsize=[300,670]))
