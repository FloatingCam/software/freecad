_macroName = "CircleText2"
# -*- coding: utf-8 -*-
'''
************************************************************************
* Copyright (c)2019 Marc Cole - Floating Cameras, LLC                 *
*                                                                      *
* This file is a supplement to the FreeCAD CAx development system.     *
*                                                                      *
* This program is free software; you can redistribute it and/or modify *
* it under the terms of the GNU Lesser General Public License (LGPL)   *
* as published by the Free Software Foundation; either version 2 of    *
* the License, or (at your option) any later version.                  *
* for detail see the LICENCE text file.                                *
*                                                                      *
* This software is distributed in the hope that it will be useful,     *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU Library General Public License for more details.                 *
*                                                                      *
* You should have received a copy of the GNU Library General Public    *
* License along with this macro; if not, write to the Free Software    *
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 *
* USA                                                                  *
*                                                                      *
************************************************************************
'''
# <editor-fold desc="imports..." >
import Part, Draft
from math import cos, sin, degrees, radians

from FCDialog_ import DFieldDef, Ui_Dialog, ShowDialog
# </editor-fold imports>

# <editor-fold desc="Globals...">
# ############################################################################################
# ##   GLOBALS
# ############################################################################################
text_extrudes = []
# text_shapes = []
shape_strings = []
depth = 3.0
radius = 35.0
b_angle = 180.0
d_angle = 0.0
ptext = "Your Text Here"
font_size = 8.0
font_file = "E:/FreeCAD/Fonts/timesbd.ttf"

fields = [DFieldDef(ps_name="ProjectName", ps_label="Project Name", ps_type="Line", ps_default=_macroName),
          DFieldDef(ps_name="BaseDepth", ps_label="Base Depth", ps_type="Input",
                    ps_default="{:.2f} mm".format(depth)),
          DFieldDef(ps_name="Radius", ps_label="Radius", ps_type="Input", ps_default="{:.2f} mm".format(radius), pf_min = 0.0, pf_max = 360),
          DFieldDef(ps_name="CounterClockwise", ps_label="Counter Clockwise", ps_type="CheckBox", ps_default="False"),
          DFieldDef(ps_name="Flat", ps_label="Flat", ps_type="CheckBox", ps_default="False"),
          DFieldDef(ps_name="BindingAngle", ps_label="Binding Angle", ps_type="Input",
                    ps_default="{}".format(b_angle)),
          DFieldDef(ps_name="Text", ps_label="Text", ps_type="Text", ps_default=ptext),
          DFieldDef(ps_name="FontSize", ps_label="Font Size", ps_type="Input",
                    ps_default="{:.2f} mm".format(font_size)),
          DFieldDef(ps_name="FontFile", ps_label="Font File", ps_type="Line", ps_default=font_file, pi_flags=1)
          # DFieldDef(ps_name="OutputDir", ps_label="Output Directory", ps_type="Line", ps_default=workdir, pi_flags=2)
          ]
# ############################################################################################
# ##   end GLOBALS
# ############################################################################################
# </editor-fold Globals>

# <editor-fold desc="Support Classes..." >
class ShapeStringE(object) :
    def __init__(self, ps_id, ps_label, po_char_, pi_index):
        self.shapeStringID = ps_id
        self.shapeStringLabel = ps_label
        self.char_ = po_char_
        self.index = pi_index
# </editor-fold Support Classes>

class MyProject(Ui_Dialog) :
    # <editor-fold desc="Support Methods...">
    def extrudeChar(self, ps_text, pi_index, po_char_, pf_depth, ps_ssid):
        extrudeName = "{}_{}".format(ps_text, "Flat" if pi_index < 0 else ps_text[pi_index])

        extru_ = self.app.ActiveDocument.addObject('Part::Extrusion', extrudeName)  #
        extru_.Label = extrudeName
        extru_.Base = po_char_

        extru_.DirMode = "Normal"
        extru_.DirLink = None
        extru_.LengthFwd = pf_depth  # 0.000000000000000
        extru_.LengthRev = 0.000000000000000  # layerNames[0][1]
        extru_.Solid = True
        extru_.Reversed = False  # if j == 1 else True
        extru_.Symmetric = False
        extru_.TaperAngle = 0.000000000000000
        extru_.TaperAngleRev = 0.000000000000000

        extru_.Base.ViewObject.hide()

        guie = self.gui.ActiveDocument.getObject(extrudeName)
        guio = self.gui.ActiveDocument.getObject(ps_ssid)

        if guio :
            guie.ShapeColor = guio.ShapeColor
            guie.LineColor = guio.LineColor
            guie.PointColor = guio.PointColor

        self.app.ActiveDocument.recompute()

        return extru_

    def circle_extrude(self, text_extrudes,  pradius, counterClockwise=False):
        global b_angle
        global d_angle
        global ptext
        p_angle = 0
        # is_fixed = True
        #c_angle = b_angle / len(text_extrudes)
        for shape in text_extrudes :
            p_angle = p_angle + (d_angle / 2.0)

            pa =  180 + p_angle if counterClockwise else 180 - p_angle
            x = cos(radians(pa)) * pradius
            y = sin(radians(pa)) * pradius

            ca = p_angle - 90 if counterClockwise else 90 - p_angle
            plm = self.app.Placement(self.app.Vector(x,y,0),self.app.Rotation(self.app.Vector(0,0,1),ca ))
            shape.Placement=plm

            p_angle += d_angle / 2.0    # Move to end of character in prep of next character

        self.app.ActiveDocument.recompute()

        union_name = "{}_CircleC".format(self.getValue("Text")) if counterClockwise else "{}_Circle".format(self.getValue("Text"))
        union = self.app.ActiveDocument.addObject("Part::MultiFuse", union_name)
        union.Shapes = text_extrudes
        ra = 180 - b_angle if counterClockwise else b_angle - 180
        union.Placement.Rotation = self.app.Rotation(self.app.Vector(0, 0, 1),  ra / 2 )
    # </editor-fold Support Methods>

    # ##############################################################
    # Override -> makeIt()
    def makeIt(self) :
        # <editor-fold desc="-> Connect to Global declarations...">
        global ptext
        global font_size
        global font_file
        global radius
        global b_angle
        global d_angle
        global base_depth
        global text_extrudes
        global shape_strings
        global shapeStringID; shapeStringID = "ShapeString"
        global shapeStringCount; shapeStringCount = 0
        # </editor-fold Global declarations>

        # <editor-fold desc="Prep and cleanup before staring the process...">
        docName = self.getValue("ProjectName")
        self.close_if_open(ps_docName=docName)
        # </editor-fold Prep...>

        # <editor-fold desc="->Process the dialog...">
        # =====================================================================================
        # =====================================================================================

        ptext = self.getValue("Text")
        font_size = float(self.getValue("FontSize"))
        font_file = self.responses["FontFile"]
        radius = float(self.getValue("Radius"))
        b_angle = float(self.getValue("BindingAngle"))
        d_angle = b_angle / len(ptext)
        base_depth = float(self.getValue("BaseDepth"))

        flat = self.getValue("Flat")

        if b_angle == 0.0 or flat :
            print ("shapeStringID: {}".format(shapeStringID))

            shapeStringName = "{}_Flat_".format(ptext)

            str_ = Draft.makeShapeString(String=ptext, FontFile=font_file, Size=font_size,
                              Tracking=0)
            str_.Label = shapeStringName

            extru_ = self.extrudeChar(ps_text=ptext, pi_index=-1, po_char_=str_, pf_depth=base_depth,
                                      ps_ssid=shapeStringID)
            shapeStringCount += 1

            self.app.ActiveDocument.recompute()
            bb = extru_.Shape.BoundBox
            extru_.Placement.Base.x = -bb.XLength / 2
            extru_.Placement.Base.y = -bb.YLength / 2

            return

        direction = self.getValue("CounterClockwise")

        inset = 0.40

        pradius = radius - inset + font_size/2 if direction else radius + inset - (font_size / 2)

        d_angle = b_angle / len(ptext)

        group_c = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", "{}_".format(ptext))
        ptext_XLength = 0.0

        for i in range(0, len(ptext)) :
            shapeStringName = "{}_{}_".format(ptext,ptext[i])

            char_ = Draft.makeShapeString(String=ptext[i], FontFile=font_file, Size=font_size,
                              Tracking=0)
            char_.Label = shapeStringName  #"{}_{}_".format(ptext,ptext[i])
            print("shapeStringID: {}".format(shapeStringID))
            shape_strings.append(ShapeStringE(ps_id=shapeStringID,
                                              ps_label=char_.Label,
                                              po_char_ = char_,
                                              pi_index = i)
                                 )

            ptext_XLength += char_.Shape.BoundBox.XLength

            char_.Placement.Base.x = -char_.Shape.BoundBox.XLength / 2
            #char_.Placement.Base.y = -char_.Shape.BoundBox.YLength / 2

            group_c.addObject(char_)

            shapeStringCount += 1
            shapeStringID = "ShapeString" + '%03d' % shapeStringCount

        self.app.ActiveDocument.recompute()


        group_e = self.app.ActiveDocument.addObject("App::DocumentObjectGroup", "{}".format(ptext))
        # Generate a set of extrusions
        te = []
        for i in range(0, len(shape_strings))  :
            ts = shape_strings[i]
            extru_ = self.extrudeChar(ps_text=ptext, pi_index=i, po_char_=ts.char_, pf_depth=base_depth, ps_ssid = ts.shapeStringID)
            group_e.addObject(extru_)
            te.append(extru_)

        self.circle_extrude(pradius=pradius, text_extrudes=te, counterClockwise=direction)

        # =====================================================================================
        # =====================================================================================
        # </editor-fold makeIt() Process the completed dialog>

# --------------
ShowDialog(po_DialogDef=MyProject(ps_name=_macroName, pl_fields=fields, po_App=App, po_Gui=Gui))