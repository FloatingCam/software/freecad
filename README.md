# FreeCAD

FreeCad Macros and support

FreeCAD 0.18 - Update ==> CoasterMaker4.py  ![Coaster Maker Image](./images/CoasterMaker.jpg)




## FCDialog_.py

Base class for providing a simple dialog with state.

## CoasterMaker3.FCMacro = FreeCAD Macro that generates circular text and creates (FreeCAD 0.17)
Easily create 3 Materials promotional or memorial coaster for holding drinks. While I demonstrated how I use it with my Original Prusa Multiple-Materials-Unit 2.0 it creates STLs ready for any multiple extusion solution.  

[![Video Overview](http://img.youtube.com/vi/_sdCdhmzq5Q/0.jpg)](http://www.youtube.com/watch?v=_sdCdhmzq5Q "")
 
CircleText.FCMacro (Previous example for FCDialog.py but not compatible with current FCDialog.  Left for reference.)

Signage.FCMacro - Premilinary work with writing a Macro and extruding text.
Left in place for reference.


